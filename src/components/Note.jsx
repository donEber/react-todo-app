import React from 'react'

const Note = (props) => (
    <div className="note">
        <p> <b>{props.title}</b> </p>
        <div>
            {props.content}
        </div>
        <div>
        <select name="type" id="note">
            <option value="ToDo">Todo</option>
            <option value="Doing">Doing</option>
            <option value="Done">Done</option>
        </select>
        <button>Update</button>
        </div>
    </div>
)
export default Note
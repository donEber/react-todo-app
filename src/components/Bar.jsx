import React from 'react'
import {Link} from 'react-router-dom'
const Bar = () => (
    <nav>
        <Link to='/'>
            Home 
        </Link>
        <Link to='/about'>
            About us 
        </Link>
        
    </nav>
)

export default Bar;
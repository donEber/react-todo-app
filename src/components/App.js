import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom';
import Bar from './Bar'
import Home from './Home/Index'

const About = () =>(<h1>About us</h1>)

const App = () => (
    <BrowserRouter >
        <Bar />
        <div className="margin">
            <Route exact path='/' component={Home} /> 
            <Route exact path='/about' component={About} /> 
        </div>
    </BrowserRouter>
)

export default App;
import React from 'react'

const NewNote = props => {
    return (
        <center>
            <h2>New Task</h2>
            <form onSubmit={props.handleSubmit}>
                <label htmlFor="title">Title</label>
                <input name="title" id="title" type="text" value={props.formValues.title} onChange={props.onChange} />
                <br />
                <br />
                <label htmlFor="content">Content</label>
                <input name="content" id="content" type="text" value={props.formValues.content} onChange={props.onChange} />
                <br />
                <br />
                <button type="submit" onClick={props.onSubmit}>Create task</button>
            </form>
        </center>
    )
}
export default NewNote
import React from 'react'
import '../index.css'
import Note from './Note'

const footer = <footer>Footer</footer>

const Card = (props) =>
    <div className="card flex-element">
        <div>
            <center>
                <h1>{props.title}</h1>
            </center>
            {
                props.notes.map(note=><Note key={note.id} title={note.title} content={note.content} />)
            }
        </div>
    </div>
export default Card
import React from 'react'
import axios from 'axios'
import Card from '../Card';
import NewNote from '../NewNote';

class Home extends React.Component {

    state = { notes: [], loading: true, error: null, urlAPI: 'https://localhost:44350/api/tasks', form: { title: '', content: '', state: 'todo' } }

    handleChange = e => {
        this.setState(
            { ...this.state, form: { ...this.state.form, [e.target.name]: e.target.value } }
        )
    }

    handleSubmit = async e => {
        e.preventDefault()
        try {
            const res = await axios.post(this.state.urlAPI, this.state.form)
            this.gettingApiData()
        } catch (error) {
            console.log(error);
        }
    }
    async gettingApiData() {
        /* getting the data */
        this.setState({ notes: [], loading: true, error: null })
        try {
            const notesFromApi = await axios.get(this.state.urlAPI).then(notes => notes.data)
            this.setState({ notes: notesFromApi, loading: false, error: null })
        } catch (error) {
            this.setState({ loading: false, error: error })
        }
    }
    componentDidMount() {
        this.gettingApiData()
    }
    render() {
        if (this.state.error) {
            return `Error ${this.state.error.message}`
        }
        return (
            <React.Fragment>
                <h1>I am the home</h1>
                <NewNote onChange={this.handleChange} formValues={this.state.form} onSubmit={this.handleSubmit} />
                {this.state.loading && (
                    <h1>Loading...</h1>
                )}
                <div className="flex-container">
                    <Card title="Todo" notes={this.state.notes.filter(note => note.state === 'todo')} />
                    <Card title="Doing" notes={this.state.notes.filter(note => note.state === 'doing')} />
                    <Card title="Done" notes={this.state.notes.filter(note => note.state === 'done')} />
                </div>
            </React.Fragment>
        )
    }
}

export default Home